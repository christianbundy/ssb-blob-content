const pull = require('pull-stream')
const test = require('tape')

const createSbot = require('scuttlebot')
  .use(require('ssb-blobs'))
  .use(require('../'))

const alice = createSbot({
  temp: 'alice',
  port: 45451,
  host: 'localhost',
  timeout: 20001,
  replicate: {hops: 100, legacy: false}
})

test('attempt to write invalid hash', t => {
  alice.publish({ type: 'blob', blob: '&abc' }, (err, msg) => {
    t.end(err)
  })
})

test('attempt to write string content-blob', t => {
  alice.blobContent.publish(false, err => {
    t.ok(err)
    t.end()
  })
})

const obj = { type: 'test' }

test('write valid blob', t => {
  alice.blobContent.publish(obj, err =>
    t.end(err)
  )
})

test('read', function (t) {
  pull(
    alice.createFeedStream(),
    pull.collect(function (err, msgs) {
      t.error(err)

      const hasBlobContent = msgs.some(msg => {
        return msg.value.meta && msg.value.meta.blobContent === true
      })
      t.ok(hasBlobContent, 'has blob-content metadata')

      const hasCorrectContent = msgs.some(msg => {
        if (msg.value.content.type === obj.type) {
          return true
        }
      })
      t.ok(hasCorrectContent, 'has correct content')

      t.end()
    })
  )
})

test.onFinish(() => alice.close())
