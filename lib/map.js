const debug = require('debug')('ssb:blob-content:map')
const pull = require('pull-stream')

const util = require('./util')

module.exports = sbot => (msg, cb) => {
  if (!util.isBlobContent(msg.value.content)) {
    return cb(null, msg)
  }

  const hash = msg.value.content.blob

  sbot.blobs.has(hash, function (err, has) {
    if (err) {
      debug('invalid hash for %s: %s', msg.key, hash)
      cb(null, msg)
    }

    if (has) {
      debug('have hash for %s: %s', msg.key, hash)
      pull(sbot.blobs.get(hash), pull.collect((err, bufs) => {
        if (err) cb(err)

        const contentString = Buffer.concat(bufs)
        try {
          const blobContent = JSON.parse(contentString)

          const original = {
            content: msg.value.content
          }

          // Back up original values in `msg.value.meta`
          if (!msg.value.meta) {
            msg.value.meta = { original }
          } else if (!msg.value.meta.original) {
            msg.value.meta.original = original
          } else if (!msg.value.meta.original.content) {
            msg.value.meta.original.content = original.content
          }

          msg.value.meta.blobContent = true
          msg.value.content = blobContent
        } catch (e) {
          debug('Invalid JSON? %O', {
            key: msg.key,
            blob: hash,
            content: contentString,
            error: e
          })
        }
        cb(null, msg)
      }))
    } else {
      debug('want hash for %s: %s', msg.key, hash)
      sbot.blobs.want(hash, (err, res) => {
        if (err) debug('ssb-blob-content received', err.message)
        cb(null, msg)
      })
    }
  })
}
