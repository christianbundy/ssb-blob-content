const isObject = input => typeof input === 'object'

const isBlobContent = content => 
  content.type === 'blob' &&
    isString(content.blob) &&
    content.blob.indexOf('&') === 0

const isString = input => typeof input === 'string'

module.exports = {
  isObject,
  isBlobContent,
  isString
}
