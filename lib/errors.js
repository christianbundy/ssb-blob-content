module.exports = {
  contentNotObject: 'Content must be an object, cannot publish non-object.'
}
