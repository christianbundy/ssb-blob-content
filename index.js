const pull = require('pull-stream')
const debug = require('debug')('ssb:blob-content:index')

const map = require('./lib/map')
const util = require('./lib/util')
const errors = require('./lib/errors')

exports.init = function (sbot) {
  sbot.addMap(map(sbot))

  return {
    publish: (obj, cb) => {
      if (!util.isObject(obj)) {
        return cb(errors.contentNotObject)
      }

      const str = JSON.stringify(obj)
      const buf = Buffer.from(str, 'utf8')

      pull(
        pull.values([buf]),
        sbot.blobs.add((err, hash) => {
          if (err) return cb(err)

          sbot.blobs.push(hash)
          sbot.publish({
            type: 'blob',
            blob: hash
          }, cb)
        })
      )
    }
  }
}
exports.manifest = { publish: 'async' }
exports.name = 'blobContent'
exports.version = require('./package.json').version
